import xml.etree.ElementTree as ET
import json


# hack that parses the /stfc-libs/udp_core/src/vhdl/generated/udp_core_top_ic_memory_map_output.xml
# and generates a param_map json register definition for the UDP core used by param_parse.py

tree = ET.parse("udp_core_top_ic_memory_map_output.xml")
root = tree.getroot() 

repl_list = [("absolute_offset","address"),("id","label"),("permission","type"),("hw_rst","default"),("description","desc"),("size","vec")]
remv_list = ["absolute_id","address","mask","tcl"]

def dict_remove(dct, remv_list):
    for rem in remv_list:
        if rem in dct:
            del dct[rem]

def dict_replace(dct, repl_list):
    for repl in repl_list:
        if repl[0] in dct:
            dct[repl[1]] = dct[repl[0]]
            del dct[repl[0]]

def clean_vec(dct):
    if int(dct["vec"]) < 2 :
        del dct["vec"]
            
def clean_type(dct):
    if dct["type"] == "r":
        dct["type"] = "RO"
    if dct["type"] == "rw":
        dct["type"] = "RW"
            
def clean_default(dct):
    if "default" in dct:
        if dct["default"] == "no":
            del dct["default"]
            return
        value = int(dct["default"], 16)
        dct["default"] = f"{value:#0{10}x}"

def clean_address(dct):
    value = int(dct["address"], 16)
    dct["address"] = f"{value:#0{10}x}"
        
reg_list = []
for item in root:
    for child in item:
        dict_remove(child.attrib, remv_list)
        dict_replace(child.attrib, repl_list)
        clean_vec(child.attrib)
        clean_type(child.attrib)
        clean_default(child.attrib)
        clean_address(child.attrib)
        reg_list.append(child.attrib)

param_map = {'project name':'Detector Group Readout Master',
               'space full name' : 'UDP core Register Space (Master)',
               'space label'     : 'udp_regs_mst',
               'version'	 : 'A1.0',
               'axi_library'     : 'dgro_fea',
               'addr bus width'  : '32',
               'address offset'  : '0xc0008000',
               'parameter map':reg_list}

param_map_json = json.dumps(param_map, indent=2)

with open("param_map_udp_core.json", "w") as outfile:
    outfile.write(param_map_json)

