import argparse
import json

# parses SciCompiler RegisterFile.json and converts to FEN address map json

parser = argparse.ArgumentParser()
parser.add_argument("spacename", help="register space name")
parser.add_argument("-r", type = int, metavar="number of rings", default=1)
parser.add_argument("-n", type = int, metavar="number of nodes", default=1)
parser.add_argument("-v", type = str, metavar="reg version", help="[A-Z]M.m , i.e. A1.0" ,default="A1.0")
args = parser.parse_args()

repl_list = [("Name","label"),("Address","address"),("Description","desc")]
remv_list = ["Version","RegionSize","Category","Path","Type"]

def dict_remove(dct, remv_list):
    for rem in remv_list:
        if rem in dct:
            del dct[rem]

def dict_replace(dct, repl_list):
    for repl in repl_list:
        if repl[0] in dct:
            dct[repl[1]] = dct[repl[0]]
            del dct[repl[0]]

def clean_address(dct):
    value = dct["address"]
    dct["address"] = f"{value:#0{10}x}"

def add_type(dct):
    dct['type'] = 'RW'

with open('RegisterFile.json') as f:
    sci_regs = json.load(f)
    regs=sci_regs["Registers"]
    
for reg in regs:
    dict_remove(reg, remv_list)
    dict_replace(reg, repl_list)
    add_type(reg)
    clean_address(reg)

param_map = {"project name":f"Detector Group {args.spacename}",
               "space full name" : f"{args.spacename} Register Space ",
               "space label"     : f"{args.spacename}_slv",
               "version"	 : f"{args.v}",
               "axi_library"     : "dgro_fea",
               "addr bus width"  : "32",
               "address offset"  : "0x00000000",
               "rings"           : f"{args.r}",
               "ring space"      : "0x10000000",
               "nodes"           : f"{args.n}",
               "node space"      : "0x00800000",
               "parameter map":regs}

param_map_json = json.dumps(param_map, indent=2)

with open(f"param_map_{args.spacename}.json", "w") as outfile:
    outfile.write(param_map_json)
