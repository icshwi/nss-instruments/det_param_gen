# VHDL functions for register parsing script
# Generates VHDL entity from input json register map
# Steven Alcock, ESS Detector Group
# July 2019

import datetime
import math
import os
import sys
from pathlib import Path 
# get global functions from reg_parse.py
import param_parse as r

####################################################################################################
### C Header File Generation
####################################################################################################

# Generate C header code based on input json_data file
def c_head_gen(json_data, OUTPUT_DIR, filename):

    if not "rings" in json_data:
        print('todo: writing header')
    else:
        print('skipping front end space')
        return 1

    fout = open_c_file(json_data, OUTPUT_DIR);

    # Generate C header
    print_head(json_data, fout, filename)

    # Generate C register map
    c_head(json_data, fout)

    fout.close()

def open_c_file(data, OUTPUT_DIR):
    fout_name = Path(str(OUTPUT_DIR) + "/software/" + data["space label"] + ".h")
    fout = open(fout_name, "w")

    print("Generating C header file for register addresses in file " + str(os.path.abspath(Path(fout_name))))

    return fout


def c_head(json_data, fout):
    base_addr_off = json_data["address offset"]
    for entry in json_data["register map"]:
        if "address" in entry:
            print(f"{entry}")
            addr = int(base_addr_off,16)+int(entry["address"],16)
        else:
            addr = int(vhdlhex_to_pythonhex(entry["addr"]), 0)
        r.text_out("#define " + "ADDR_" + entry["label"].upper() + " 0x" + format(addr, '08x'), fout)


# Generate C header
def print_head(json_data, fout, filename):
    # metadata
    r.text_out("// Register map generated at: " + datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"), fout)
    r.text_out("// using: param_parse.py", fout)
    r.text_out("// Register definition file: " + str(filename), fout)
    r.text_out("// Project: " + json_data["project name"], fout)
    r.text_out("// Register space: " + json_data["space full name"], fout)
    r.text_out("", fout)

def vhdlhex_to_pythonhex(string):
    var = "0x" + string[2:-1]
    return var

