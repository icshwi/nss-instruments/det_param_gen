import datetime
import os
import sys
from pathlib import Path 
# get global functions from reg_parse.py
import param_parse as r

def ctl_py_gen(json_data, OUTPUT_DIR, filename):
    
    fout = open_txt_file(json_data, OUTPUT_DIR);

    # Generate header
    print_head(json_data, fout, filename)
    print_addr(json_data, fout)

    fout_header = open_header_file(json_data, OUTPUT_DIR)
    print_cpp_addr(json_data, fout_header)

####################################################################################################
### Python Helper Functions
### TODO: commonality with vhdl_entity_gen?
####################################################################################################

# create output text file for register map
def open_txt_file(data,OUTPUT_DIR):

    if "version" in data.keys():
        version_suffix = "_" + data["version"].replace(".","v")  # replace '.' with 'v' as will be part of filename
    else:
        version_suffix = ""

    fout_name = Path(str(OUTPUT_DIR) + "/address_map/" + data["space label"] + "_map" + version_suffix + ".txt")
    fout = open(fout_name, "w")

    print("Generating text register map for Python slow control in file " + str(os.path.abspath(Path(fout_name))))

    return fout


def print_head(json_data, fout, filename):
    r.text_out("# Register map generated at: " + datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"), fout)
    r.text_out("# using: " + str(filename), fout)
    r.text_out("# Register definition file: " + sys.argv[1], fout)
    r.text_out("# Project: " + json_data["project name"], fout)
    r.text_out("# Register space: " + json_data["space full name"], fout)
    r.text_out("", fout)

def print_addr(json_data, fout):

    addr_off = 0
    base_addr_off = json_data["address offset"]
    prefix = ""

    if "rings" in json_data:
        rings = int(json_data["rings"], 0)
        ring_label = 1
    else:
        rings = 1
        ring_label = 0

    if "nodes" in json_data:
        nodes = int(json_data["nodes"], 0)
        node_label = 1
    else:
        nodes = 0
        node_label = 0

    for i in range(rings):
        # generate address maps for the number of slaves from the JSON, plus for the default slave "31" 
        # that the front-ends intialise to on power up/reset
        # For RMM register maps, just generate one lot of registers ("31")
        node_list = list(range(nodes))
        node_list.append(31)
        for j in node_list:

            if ring_label:
                prefix = "_" + str(i)
                addr_off = i * int(json_data["ring space"], 0)
                if node_label:
                    prefix += "_" + str(j)
                    addr_off += j * int(json_data["node space"], 0)
            for entry in json_data["register map"]:
                if "address" in entry:  # taking address from json if it exists
                    addr = int(base_addr_off,16)+addr_off+int(entry["address"],16)
                else:
                    addr = int(vhdlhex_to_pythonhex(entry["addr"]), 0) + addr_off
                r.text_out(entry["label"] + prefix + " 0x" + format(addr, '08x') + " LW", fout)

def vhdlhex_to_pythonhex(string):
    var = "0x" + string[2:-1]
    return var
    
def print_cpp_addr(json_data, fout):

    addr_off = 0
    prefix = ""

    if "rings" in json_data:
        rings = int(json_data["rings"], 0)
        ring_label = 1
    else:
        rings = 1
        ring_label = 0

    if "nodes" in json_data:
        nodes = int(json_data["nodes"], 0)
        node_label = 1
    else:
        nodes = 1
        node_label = 0

    r.text_out("#include <map>", fout)
    r.text_out("#include <string>", fout)
    r.text_out("void setRegisterValues(std::map<std::string, uint32_t> &registers) {\n", fout)

    for entry in json_data["register map"]:
        addr = int(vhdlhex_to_pythonhex(entry["addr"]), 0)
        r.text_out("registers[\"" + entry["label"] +"\"]=0x" + format(addr, '08x') + ";", fout)
    r.text_out("}", fout)


# create output header file for C++ slow control GUI
def open_header_file(data,OUTPUT_DIR):
    fout_name = Path(str(OUTPUT_DIR) + "/address_map/" + data["space label"] + "_map.h")
    fout = open(fout_name, "w")

    print("Generating header file register map for C++ slow control in file " + str(os.path.abspath(Path(fout_name))))

    return fout
